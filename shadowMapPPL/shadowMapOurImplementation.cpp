#include<Windows.h>
#include<GL/glew.h>

#include<gl/GL.h>

#include<stdio.h>
#include"../vmath.h"
using namespace vmath;
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#define WIN_WIDTH 1280
#define WIN_HEIGHT 720

FILE * gpfile;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool GBActiveWindow = false;
bool bFullScreen = false;
DWORD dwStyle;
HWND ghWnd = NULL;


//for debug depth quad shaders
GLuint gVertexShaderObjectDebug;
GLuint gFragmentShaderObjectDebug;
GLuint gShaderProgramObjectDebug;

//for simple depth shaders
GLuint gVertexShaderObjectSimpleDepth;
GLuint gFragmentShaderObjectSimpleDepth;
GLuint gShaderProgramObjectSimpleDepth;


enum
{

	AMC_ATRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

GLuint planeVao;
GLuint planeVbo;
GLuint depthMapFBO;

GLuint cubeVao;
GLuint cubeVbo;

GLuint quadVao;
GLuint quadVbo;

GLuint mvpUniform;
GLuint samplerUniform;
GLuint lightSpaceMatrixUniform;
GLuint modelMatrixUniform;
//for texture 
GLuint textureDepthMap;
mat4 orthographicProjectionMatrix;
const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
void ToggleFullScreen(void);
void uninitialize(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//function declaration
	int initialize(void);
	void display(void);


	//variable declaration
	bool bDone = false;
	int iRet = 0;
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("PPL-Perspective-Triangle");

	if (fopen_s(&gpfile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
	}
	else
	{
		fprintf(gpfile, "Log File Created \n");
	}

	//intialization on wndclassex
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;

	//register class
	RegisterClassEx(&wndclass);

	//create window

	hwnd = CreateWindowEx(WS_EX_APPWINDOW, szAppName,
		TEXT("PPL-Perspective-Triangle"),

		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghWnd = hwnd;
	iRet = initialize();

	//checking errors
	if (iRet == -1)
	{
		fprintf(gpfile, "Choose Pixel Format Failed \n");
		DestroyWindow(hwnd);

	}
	else if (iRet == -2)
	{
		fprintf(gpfile, "Set Pixel Format Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpfile, "wglCreateContextFailed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpfile, "wglMakeCurrentFailed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpfile, "initialize() is successfull \n ");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (GBActiveWindow == true)
			{
			}
			display();
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//function declarations
	void resize(int, int);

	void uninitialize(void);


	//code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		GBActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		GBActiveWindow = false;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			if (bFullScreen == false)
			{
				ToggleFullScreen();
			}
			else
			{
				ToggleFullScreen();
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;

	}



	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
int initialize()
{
	void resize(int, int);
	void initDebugDepthQuadShaders();
	void initSimpleDepthShaders();
	//variable declarations

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLenum result;
	//code
	//intialize pfd structure

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	ghdc = GetDC(ghWnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL)
	{
		return(-3);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpfile, "glew init failed");
		uninitialize();
		DestroyWindow(ghWnd);

	}

	initDebugDepthQuadShaders();
	initSimpleDepthShaders();


	//depth enabling
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

	orthographicProjectionMatrix = mat4::identity();
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}

void initDebugDepthQuadShaders()
{
	gVertexShaderObjectDebug = glCreateShader(GL_VERTEX_SHADER);

	//vertex shader code 
	const  GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 vPosition;" \
		"in vec2 vTexCoord;" \
		"out vec2 out_texcoord;" \
		"void main(void)" \
		"{" \
		"gl_Position = vec4(vPosition,1.0);" \
		"out_texcoord = vTexCoord;" \
		"}";

	//specify above shader source code to vertexShaderObject 
	//give shader source code
	glShaderSource(gVertexShaderObjectDebug, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//compile the vertex shader code 
	glCompileShader(gVertexShaderObjectDebug);

	//error checking code for vertex shader 
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectDebug, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectDebug, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectDebug, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Vertex Shader Compilation Log :%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

	//WRITE fragment shader 
	gFragmentShaderObjectDebug = glCreateShader(GL_FRAGMENT_SHADER);

	//shader code
	const GLchar* fragmentShaderSourceCode =
		"#version 450" \
		"\n" \
		"out vec4 FragColor;" \
		"in vec2 out_texcoord;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
		"float depthValue = texture(u_sampler, out_texcoord).r;" \
		"FragColor = vec4(vec3(depthValue),1.0);" \
		"}";

	//specify above shader source code to fragmentShaderObject
	//give shader source code
	glShaderSource(gFragmentShaderObjectDebug, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);

	//COMpile fragment shader code
	glCompileShader(gFragmentShaderObjectDebug);

	//error checking code for shader 
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObjectDebug, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectDebug, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectDebug, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Fragment Shader Compilation Log:%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);

			}
		}
	}

	//create shader program object
	gShaderProgramObjectDebug = glCreateProgram();

	//attach vertex shader to shader program 
	glAttachShader(gShaderProgramObjectDebug, gVertexShaderObjectDebug);

	//attach fragment shader to  shader program 
	glAttachShader(gShaderProgramObjectDebug, gFragmentShaderObjectDebug);

	glBindAttribLocation(gShaderProgramObjectDebug, AMC_ATRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectDebug, AMC_ATTRIBUTE_TEXCOORD0, "vTexCoord");

	//NOW Link shader program 
	glLinkProgram(gShaderProgramObjectDebug);


	//ERROR CHECKING FOR SHADER program 
	GLint iProgramLinkStaus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectDebug, GL_LINK_STATUS, &iProgramLinkStaus);

	if (iProgramLinkStaus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectDebug, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectDebug, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Shader Program Link log:-%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

	//mvpUniform = glGetUniformLocation(gShaderProgramObjectDebug, "u_mvp_matrix");
	samplerUniform = glGetUniformLocation(gShaderProgramObjectDebug, "u_sampler");
	

	
	//depth Map FBO
	glGenFramebuffers(1, &depthMapFBO);
	//create depth texture
	glGenTextures(1, &textureDepthMap);
	glBindTexture(GL_TEXTURE_2D, textureDepthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT,
		0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//attach depth texture as FBO's depth buffer 
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepthMap, 0);
	glDrawBuffer(GL_NONE);
	/* framebuffer is not complete witout color buffer so we are telling opengl that
	WE ARE NOT GOING TO RENDER ANY COLOR DATA*/
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	GLfloat quadVertices[] =
	{
		// positions        // texture Coords
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	};


	//for quad
	glGenVertexArrays(1, &quadVao);
	glBindVertexArray(quadVao);

	glGenBuffers(1, &quadVbo);
	glBindBuffer(GL_ARRAY_BUFFER, quadVbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (void*)0);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 5, (void*)(sizeof(GLfloat) * 3));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}
void initSimpleDepthShaders()
{
	gVertexShaderObjectSimpleDepth = glCreateShader(GL_VERTEX_SHADER);

	//vertex shader code 
	const  GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 vPosition;" \
		"uniform mat4 u_lightSpaceMatrix;" \
		"uniform mat4 u_model;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_lightSpaceMatrix * u_model * vec4(vPosition,1.0);" \
		"}";

	//specify above shader source code to vertexShaderObject 
	//give shader source code
	glShaderSource(gVertexShaderObjectSimpleDepth, 1, (const GLchar * *)& vertexShaderSourceCode, NULL);

	//compile the vertex shader code 
	glCompileShader(gVertexShaderObjectSimpleDepth);

	//error checking code for vertex shader 
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar* szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectSimpleDepth, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectSimpleDepth, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectSimpleDepth, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Vertex Shader Compilation Log :%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

	//WRITE fragment shader 
	gFragmentShaderObjectSimpleDepth = glCreateShader(GL_FRAGMENT_SHADER);

	//shader code
	const GLchar* fragmentShaderSourceCode =
		"#version 450" \
		"\n" \
		"void main(void)" \
		"{" \
			// gl_FragDepth = gl_FragCoord.z;
		"}";

	//specify above shader source code to fragmentShaderObject
	//give shader source code
	glShaderSource(gFragmentShaderObjectSimpleDepth, 1, (const GLchar * *)& fragmentShaderSourceCode, NULL);

	//COMpile fragment shader code
	glCompileShader(gFragmentShaderObjectSimpleDepth);

	//error checking code for shader 
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObjectSimpleDepth, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectSimpleDepth, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectSimpleDepth, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Fragment Shader Compilation Log:%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);

			}
		}
	}

	//create shader program object
	gShaderProgramObjectSimpleDepth = glCreateProgram();

	//attach vertex shader to shader program 
	glAttachShader(gShaderProgramObjectSimpleDepth, gVertexShaderObjectSimpleDepth);

	//attach fragment shader to  shader program 
	glAttachShader(gShaderProgramObjectSimpleDepth, gFragmentShaderObjectSimpleDepth);

	glBindAttribLocation(gShaderProgramObjectSimpleDepth, AMC_ATRIBUTE_POSITION, "vPosition");

	//NOW Link shader program 
	glLinkProgram(gShaderProgramObjectSimpleDepth);


	//ERROR CHECKING FOR SHADER program 
	GLint iProgramLinkStaus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObjectSimpleDepth, GL_LINK_STATUS, &iProgramLinkStaus);

	if (iProgramLinkStaus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectSimpleDepth, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectSimpleDepth, iInfoLogLength, &written, szInfoLog);
				fwprintf(gpfile, TEXT("Shader Program Link log:-%hs\n"), szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

	lightSpaceMatrixUniform = glGetUniformLocation(gShaderProgramObjectSimpleDepth, "u_lightSpaceMatrix");
	modelMatrixUniform = glGetUniformLocation(gShaderProgramObjectSimpleDepth, "u_model");
	GLfloat planeVertices[] =
	{
		// positions            // normals         // texcoords
			 25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,   0.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,

			25.0f, -0.5f,  25.0f,  0.0f, 1.0f, 0.0f,  25.0f,  0.0f,
			-25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,   0.0f, 25.0f,
			 25.0f, -0.5f, -25.0f,  0.0f, 1.0f, 0.0f,  25.0f, 10.0f
	};
	//create vao
	glGenVertexArrays(1, &planeVao);
	glBindVertexArray(planeVao);

	glGenBuffers(1, &planeVbo);
	glBindBuffer(GL_ARRAY_BUFFER, planeVbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), planeVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)0);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
	GLfloat vertices[] =
	{
		// back face
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
			// front face
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			// left face
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			// right face
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
			// bottom face
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			// top face
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			 1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
			 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
	};

	glGenVertexArrays(1, &cubeVao);
	glBindVertexArray( cubeVao);

	glGenBuffers(1, &cubeVbo);
	glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);

	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)0);
	glEnableVertexAttribArray(AMC_ATRIBUTE_POSITION);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)(sizeof(GLfloat) * 3));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 8, (void*)(sizeof(GLfloat) * 6));
	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
	glBindVertexArray(0);

}

void resize(int width, int height)

{
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	//perspectiveProjectionMatrix=perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	orthographicProjectionMatrix = ortho(-10.0f, 10.0f, -10.0f, 10.0f, 1.0f, 7.5f);
	

}

void display(void)
{
	void drawFromLight();
	drawFromLight();
	
	glViewport(0, 0, WIN_WIDTH, WIN_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObjectDebug);
		
	glUniform1i(samplerUniform, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureDepthMap);
	glBindVertexArray(quadVao);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
	//declaration of matrices
	//mat4 modelViewMatrix;
	//mat4 modelViewProjectionMatrix;
	//mat4 lightProjection;
	

	//modelViewMatrix = mat4::identity();
	//modelViewProjectionMatrix = mat4::identity();
	
	//render scene from light point of view 
	//modelViewMatrix = translate(0.0f, 1.5f, 0.0f);
	//modelViewProjectionMatrix = orthographicProjectionMatrix * modelViewMatrix;
	//send necessary matrices to shader in respective uniform

	//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

/*	glUniform1i(samplerUniform, 0);
	//bind with vao
	glBindVertexArray(planeVao);
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindVertexArray(0);*/
	glUseProgram(0);

	SwapBuffers(ghdc);
}


void drawFromLight()
{	// 1. render depth of scene to texture (from light's perspective)

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObjectSimpleDepth);

	vec3 lightPos(-2.0f, 4.0f, -1.0f);
	mat4 lightView;
	mat4 lightSpaceMatrix;
	mat4 modelMatrix;
	lightView = mat4::identity();
	lightSpaceMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	lightView = lookat(lightPos, vec3(0.0f), vec3(0.0, 1.0, 0.0));
	lightSpaceMatrix = orthographicProjectionMatrix * lightView;
	glUniformMatrix4fv(lightSpaceMatrixUniform, 1, GL_FALSE, lightSpaceMatrix);
	
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	//draw floor
	modelMatrix = mat4::identity();
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);
	glBindVertexArray(planeVao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
	//draw cubes
	modelMatrix = mat4::identity();
	mat4 translationMatrix;
	mat4 scaleMatrix;
	
	//first cube
	translationMatrix = translate(0.0f, 1.5f, 0.0f);
	scaleMatrix = scale(0.5f, 0.5f, 0.5f);
	modelMatrix = translationMatrix * scaleMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(cubeVao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	//second cube
	modelMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();

	translationMatrix = translate(2.0f, 0.0f, 1.0f);
	scaleMatrix = scale(0.5f, 0.5f, 0.5f);
	modelMatrix = translationMatrix * scaleMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(cubeVao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	//third cube 
	mat4 rotationMatrix;
	modelMatrix = mat4::identity();
	translationMatrix = mat4::identity();
	scaleMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	
	translationMatrix = translate(-1.0f, 0.0f, 2.0f);
	scaleMatrix = scale(0.25f, 0.25f, 0.25f);
	rotationMatrix = rotate(60.0f, vec3(1.0f, 0.0f, 1.0f));
	//rotationMatrix = rotate(60.0f, normalize(vec3(1.0f, 0.0f, 1.0f)));

	modelMatrix = translationMatrix * scaleMatrix;
	modelMatrix = modelMatrix * rotationMatrix;
	glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, modelMatrix);

	glBindVertexArray(cubeVao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glUseProgram(0);


	







}


void uninitialize(void)
{
	if (bFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);
		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);

	}

	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;
	}

	if (gpfile)
	{
		fprintf(gpfile, "Log File Exit \n");
		fclose(gpfile);
		gpfile = NULL;
	}

	if (planeVbo)
	{
		glDeleteBuffers(1, &planeVbo);
		planeVbo = NULL;
	}

	if (planeVao)
	{
		glDeleteVertexArrays(1, &planeVao);
		planeVao = 0;
	}

	/*if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);

		//ask program how many shaders attach to you

		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint* pShader = (GLuint*)malloc(sizeof(shaderCount));

		if (pShader)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShader);

			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				glDetachShader(gShaderProgramObject, pShader[shaderNumber]);

				glDeleteShader(pShader[shaderNumber]);
				pShader[shaderNumber] = 0;
			}
			free(pShader);
		}

		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}*/




}
void ToggleFullScreen(void)
{
	MONITORINFO mi;
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);

			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);
		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}
